# Exercise 1

    Create 2 React components, one of them will be the parent component and the other will be the child component.

    The parent component will render the child component passing a prop to it (this will be your name that you first assign to the parent component using the keyword this).

    The child component needs to return the following message: Hello 'the name you passed as a prop' i am a child component!

# Exercise 2

    Create a parent component, inside which you define 2 arrays of equal length, the first will have 5 firstnames and the second 5 lastnames.
    Create 2 children components – one to display the firstnames and one to display the lastnames.

# Exercise 3 

	Create 3 components, Main, List, and Item

	In Main declare an array of numbers; you should pass this array as it is to List, 
	then in List you should map through the array and for each iteration you should pass each number to Item in which you render it.


# Exercise 4;

	Refactor the eCommerce clone you did in the previous block to use props.
	You should now use the Main, List and Item pattern with the list of products.
	Declare the list in Main and pass it to List
	Loop in List and for each iteration render the product in Item. 











