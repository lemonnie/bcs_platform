# Exercise 1

Create an input field for the user to create  a password, and a button to submit it.

You need to make sure that the password will be at least 8 characters long and that it will contain at least one uppercase character.

**Requirements**

- All your conditions must be inside the render function
- You should use both, `if` statement outside the return and the ternary operator inside it.
- there should be two separate messages: an `h1` that informs the user if the length requirements has been met and an `h2` that checks if there is at least one upper case character.

